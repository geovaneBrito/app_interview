import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { InterViewServiceProvider } from '../../providers/service/service';
//import { InterViewServiceProvider } from '../../providers/InterView-service/InterView-service';


@Component({
  selector: 'page-notificacao',
  templateUrl: 'notificacao.html',
})
export class NotificacaoPage {

  flagFeeds: boolean;
  aulas: String[];
  public feeds: Array<any>;
  public noFilter: Array<any>;
  public tabBarElement: any;
  public searchTerm: string = '';
  public has_notificacao: boolean = true;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: InterViewServiceProvider,
    public events : Events

  ) {
    events.subscribe('historico:atualiza', (time) => {
      console.log('historico:atualiza', 'at', time);      
      this.listaNotificacoes();

    });
    this.listaNotificacoes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificacaoPage');

  }


  listaNotificacoes() {
    var aluno = JSON.parse(localStorage.getItem('usuario_logado'));
    this.service.getNotificacao(aluno.id_aluno).then(data => {
      const novoArray = [

        data.aulas
      ]
      if (novoArray.length > 1){
          this.feeds = novoArray;
        }
      this.flagFeeds = true;
      this.feeds = data.aulas;

      console.log("feeds", this.feeds);
    });
  }
  doRefresh(refresher) {
    this.listaNotificacoes();
    refresher.complete();
  }

}
