import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
//import { InterViewServiceProvider } from '../../providers/InterView-service/InterView-service';
import { CalendarPage } from '../calendar/calendar';

/**
 * Generated class for the ProdutoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-produto',
  templateUrl: 'produto.html',
})
export class ProdutoPage {

  public feeds: Array<string>;
  public noFilter: Array<any>;
  public tabBarElement: any;
  public searchTerm: string = '';
  public naoExibi:boolean = true; 
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {

   // this.listaProdutos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProdutoPage');

  }

/*
  listaProdutos() {
    this.service.listaTodosProdutos().then(data => {
      console.log(data);
      if (data.produtos) {

        this.feeds = data.produtos;
        this.noFilter = this.feeds;
        console.log(this.feeds);
      }
      else {
        console.log('Erro de produto');
        let toast = this.toastCtrl.create({
          message: "Problema para consultar lista, tente novamente!",
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast erro');
        });

        toast.present();
      }

    });
  }*/
  doRefresh(refresher) {
    //this.listaProdutos();
    refresher.complete();
  }
  filterItems() {
    this.feeds = this.noFilter.filter((item) => {

      return item.nome.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });
  }

  goCalendar( id_modalidade : any){

    localStorage.setItem('id_modalidade',  id_modalidade);
    this.navCtrl.push(CalendarPage);
  }



}
