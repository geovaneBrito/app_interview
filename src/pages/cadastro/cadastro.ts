import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, App, ViewController, LoadingController } from 'ionic-angular';
import { InterViewServiceProvider } from '../../providers/service/service';
import { FormBuilder } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  public cadastroForm2: any;
  public listaPais: Array<string>;
  public emailStatus: boolean = true;
  public erroValidacao: boolean = false;
  imageURI: any;
  imageBase64: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public appCtrl: App,
    public loadingCtrl: LoadingController,
    private camera: Camera,
    public service: InterViewServiceProvider

  ) {

    this.cadastroForm2 = formBuilder.group(
      {
        nome: [''],
        email: [''],
        nascimento: [''],
        telefone: [''],
        endereco: [''],
        bairro: [''],
        cep: [''],
        cidade: [''],
        uf: [''],
        senha: [''],
        complemento: [''],
        sexo: [''],
        numero: [''],
        cadastro_rg: [''],
        cadastro_cpf: ['']
      }
    )

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Cadastro2Page');
  }

  getImage() {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.imageBase64 = "data:image/jpeg;base64," + imageData;
      //let consultor = localStorage.getItem('consultor');

    }, (err) => {
      console.log(err);
      this.mensagemToast('Problema para buscar foto, tente novamente mais tarde!');
    });
  }

  mensagemToast(mensagem) {
    let toast = this.toastCtrl.create({
      cssClass: 'class-toast',
      message: mensagem,
      duration: 5000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


  goBack() {

    this.navCtrl.pop();
  }

  salvarCadastro(params) {

    console.log(this.cadastroForm2.value);
    if (!this.cadastroForm2.valid) {
      this.mensagemToast('Favor preencher todos os campos!');
    }
    else {

      if (this.imageURI == "") {
        this.mensagemToast('Foto obrigatória!');

      } else {


        let user = {
          nome: this.cadastroForm2.value.nome, 
          email: this.cadastroForm2.value.email,
          senha: this.cadastroForm2.value.senha,
          nascimento: this.cadastroForm2.value.nascimento, 
          sexo: this.cadastroForm2.value.sexo,
          cpf: this.cadastroForm2.value.cadastro_cpf,
          rg: this.cadastroForm2.value.cadastro_rg,
          foto: this.imageBase64,
          logradouro: this.cadastroForm2.value.bairro,
          numero: this.cadastroForm2.value.numero,
          complemento: this.cadastroForm2.value.complemento,
          cidade: this.cadastroForm2.value.cidade,
          cep: this.cadastroForm2.value.cep,
          uf: this.cadastroForm2.value.uf,
          ddd: this.cadastroForm2.value.telefone.substring(1, 3),
          telefone: this.cadastroForm2.value.telefone.substring(5)
        };
        console.log('user',user);

        this.service.salvarAluno(user).then(data => {
          console.log('salvarAluno', data);
          if (data.status == false) {
            this.mensagemToast(data.message);

          } else {
            this.mensagemToast('Dados cadastros com Sucesso');
            this.navCtrl.pop();

          }
        }, (err) => {
          console.log('error', err);
          this.mensagemToast('Houve um problema com cadastro. Tente novamente!');

        });


      }



    }


  }

}
