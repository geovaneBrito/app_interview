import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { CadastroPage } from '../cadastro/cadastro';
import { InterViewServiceProvider } from '../../providers/service/service';
import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public loginForm: any;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    public service: InterViewServiceProvider
  ) {

    var validaLogin = localStorage.getItem('loginOk');
    if (validaLogin == "true")
      this.navCtrl.push(TabsPage);



    this.loginForm = formBuilder.group(
      {
        // descomentar quando for fazer validações de campos
        login: ['', Validators.required],
        senha: ['', Validators.required]

      })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');

  }
  validaLogin(params) {

    let { login, senha } = this.loginForm.controls;
    if (!this.loginForm.valid) {
      if (!login.valid) {
        console.log("login");
      }
      if (!senha.valid) {
        console.log("senha");
      }
      this.mensagemToast('Favor preencher todos os campos!');


    } else {

      /*if (login.value == "lucianoserra" && senha.value == "ab")
        this.navCtrl.push(TabsPage);
      else
        this.mensagemToast("Usuario ou senha incorretos. Tente novamente.");
*/
      this.service.validaLogin(login.value, senha.value).then(data => {

        if (data.status == false) {
          console.log('retorno', data);
          var erro = "Problema com autenticação. Tente novamente.";
          if (data.status == false) {
            erro = data.message;
          }
          this.mensagemToast(erro);

        }
        else {
          this.navCtrl.push(TabsPage);
          localStorage.setItem('usuario_logado', JSON.stringify(data));

        }

      });


    }
  }
  goToCadastro1(params) {
    if (!params) params = {};
    this.navCtrl.push(CadastroPage);

  }

  resetaSenha() {
    let { login } = this.loginForm.controls;

    if (!login.valid) {
      this.mensagemToast('Login inválido');
    } else {
      this.service.recuperar_senha(login.value).then(data => {
        if (data.status == false) {
          console.log('retorno', data);
          var erro = "Problema para trocar senha. Tente novamente.";
          if (data.status == false) {
            erro = data.message;
          }
          this.mensagemToast(erro);

        }
        else {
          this.mensagemToast(data.message);

        }
      });
    }

  }

  mensagemToast(mensagem) {
    let toast = this.toastCtrl.create({
      cssClass: 'class-toast',
      message: mensagem,
      duration: 5000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
