import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { InterViewServiceProvider } from '../../providers/service/service';
import { ProdutoPage } from '../produto/produto';

@Component({
  selector: 'page-confirmacao',
  templateUrl: 'confirmacao.html',
})
export class ConfirmacaoPage {

  usuario_logado: any;
  public localSelecionado: any;
  horaFormatada: string;
  formaDePagamento: any
  horaSelecionada: string;
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public service: InterViewServiceProvider,
    public toastCtrl: ToastController,
    public navParams: NavParams
  ) {

    this.formaDePagamento = 0;
    this.localSelecionado = JSON.parse(localStorage.getItem('localSelecionado'));
    this.usuario_logado = JSON.parse(localStorage.getItem('usuario_logado'));
    this.horaSelecionada = localStorage.getItem('horaSelecionada');
    this.horaFormatada = localStorage.getItem('horaFormatada');


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmacaoPage');
  }

  reserva_aula() {
    this.service.reserva_aula(this.horaSelecionada, this.usuario_logado.id_aluno, this.formaDePagamento).then(data => {
      console.log('data', data);
      if (data.status == true){
        this.mensagemToast(data.message);
        this.navCtrl.setRoot( ProdutoPage );
      }
      if (data.status == false)
        this.mensagemToast(data.message);


    })
  }

  mensagemToast(mensagem) {
    let toast = this.toastCtrl.create({
      cssClass: 'class-toast',
      message: mensagem,
      duration: 5000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


}
