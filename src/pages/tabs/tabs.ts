import { Component } from '@angular/core';
import { ProdutoPage } from '../produto/produto';
import { NotificacaoPage } from '../notificacao/notificacao';
import { LoginPage } from '../login/login';
import { NavController, Tab, MenuController, AlertController, Events } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ProdutoPage;
  tab3Root = NotificacaoPage;
  selectedIndex: number = 0;

  constructor(
    public navCtrl: NavController, 
    public menuCtrl: MenuController, 
    public alertCtrl: AlertController,
    public events: Events) {
  }

  openMenu() {
    console.log("Menu");
    this.menuCtrl.toggle();

  }
  // <ion-tabs (ionChange)="tabChange($event)">
  tabChange(tab: Tab) {
    this.selectedIndex = tab.index;
    console.log("Tabs change");

    //index equals 2/other to add/remove tab home click event 
    if (1 == this.selectedIndex) {

      console.log('Tabs de historico selecionado');
      this.atualizaHistorico();
    }
    if (2 == this.selectedIndex) {

      this.presentConfirm();

    }

    //this.menuCtrl.toggle();
    // Quando index da tabs home, fazer scrollToTop

  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirme o Logout',
      message: 'Deseja realmente sair?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            console.log('Buy clicked');
            localStorage.setItem('loginOk', 'false');
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }
  closeMenu() {
    this.menuCtrl.close();
  }
  
  atualizaHistorico() {
    this.events.publish('historico:atualiza', Date.now()); 

  }
}
