import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InterViewServiceProvider } from '../../providers/service/service';
import { ConfirmacaoPage } from '../confirmacao/confirmacao';


@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  localSelecionado: any;
  temp: {};
  hasAgenda: boolean;
  datas: Promise<any>;
  numero5: boolean;
  numero4: boolean;
  numero3: boolean;
  numero2: boolean;
  numero1: boolean;
  public horaSelecionada : any;
  public feeds: Array<any>;
  public id_modalidade: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: InterViewServiceProvider
  ) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
    var id_modalidade = localStorage.getItem('id_modalidade');
    this.id_modalidade = id_modalidade;
    this.service.buscaData(id_modalidade).then(data => {

      this.datas = data.dias;
      console.log('data', data.dias);
      if (data.dias.length > 0)
        this.hasAgenda = true;
    })


  }
  buscaLocal(id_data) {

    this.service.buscaAgenda(this.id_modalidade, id_data).then(data => {  

      this.feeds = data;
      console.log('this.feeds', JSON.stringify(this.feeds));
    })
  }


  selecionado(numero) {

    var selecionado = "hora_" + numero;
    let botao = document.getElementById(selecionado);

    botao.classList.toggle("selecionado");
    
  }
  salvaLocalSelecionada( local, horaFormatada )
  {
    this.localSelecionado = local;
    localStorage.setItem('localSelecionado', JSON.stringify(this.localSelecionado));
    localStorage.setItem('horaSelecionada',this.horaSelecionada);
    localStorage.setItem('horaFormatada',horaFormatada);
 
  }
  goToConfirmacao() {

    
    this.navCtrl.push(ConfirmacaoPage);
  }

}
