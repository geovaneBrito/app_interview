import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the HellooServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class InterViewServiceProvider {


  public feeds: Array<string>;
  constructor(
    public httpClient: HttpClient,
    public loadingCtrl: LoadingController
  ) { }

  recuperar_senha(login): Promise<any> {
    let url = "http://tl.upmidia.digital/index.php/webservice/recuperar_senha?login=" + login;
    let loading = this.loadingCtrl.create();

    loading.present();
    return new Promise(resolve => {
      this.httpClient.get(url).subscribe(
        res => {
          ;    // return array from json file
          loading.dismiss();
          resolve(res || []);
        },
        err => {
          loading.dismiss();
          resolve(err.error);
        }
      );
    }).catch(err => { console.log(err); loading.dismiss() });  }
  buscaData(id_modalidade): Promise<any> {
    let url = "http://tl.upmidia.digital/index.php/webservice/aulas_dias?id_modalidade=" + id_modalidade;
    let loading = this.loadingCtrl.create();

    loading.present();
    return new Promise(resolve => {
      this.httpClient.get(url).subscribe(
        res => {
             // return array from json file
          loading.dismiss();
          resolve(res || []);
        },
        err => {
          loading.dismiss();
          resolve(err.error);
        }
      );
    }).catch(err => { console.log(err); loading.dismiss() });

  }
  getNotificacao(id_aluno): Promise<any> {
    let url = "http://tl.upmidia.digital/index.php/webservice/minhas_aulas?id_aluno=" + id_aluno;
    let loading = this.loadingCtrl.create({
      content: '...'
    });

    loading.present();
    return new Promise(resolve => {
      this.httpClient.get(url).subscribe(
        res => {
          loading.dismiss();
          resolve(res || []);
        },
        err => {
          loading.dismiss();
          console.log("Erro de busca");
          resolve(err);
        }
      );
    }).catch(err => { console.log(err); loading.dismiss() });

  }
  buscaAgenda(id_modalidade, id_data): Promise<any> {
    let url = "http://tl.upmidia.digital/index.php/webservice/aulas?id_modalidade=" + id_modalidade + "&dia=" + id_data;
    let loading = this.loadingCtrl.create();

    loading.present();
    return new Promise(resolve => {
      this.httpClient.get(url).subscribe(
        res => {
          loading.dismiss();
          resolve(res || []);
        },
        err => {
          loading.dismiss();
          resolve(err.error);
        }
      );
    }).catch(err => { console.log(err); loading.dismiss() });

  }

  reserva_aula(id_aula, id_aluno, id_tipo_pagamento): Promise<any> {
    let url = "http://tl.upmidia.digital/index.php/webservice/reserva_aula?" +
      "id_aula=" + id_aula +
      "&id_tipo_pagamento=" + id_tipo_pagamento +
      "&id_aluno=" + id_aluno;

    let body = new URLSearchParams();
    body.set('id_aula', id_aula);
    body.set('id_aluno', id_aluno);
    body.set('id_tipo_pagamento', id_tipo_pagamento);

    let loading = this.loadingCtrl.create();


    loading.present();
    return new Promise(resolve => {
      this.httpClient.post(url, body.toString()
      ).subscribe(
        res => {
          loading.dismiss();
          resolve(res);
        },
        err => {
          loading.dismiss();
          resolve(err.error);
        }
      );
    }).catch(err => { console.log(err); loading.dismiss() });

    
  }

  validaLogin(login, senha): Promise<any> {
    //login=lucianoserra&senha=ab
    let url = "http://tl.upmidia.digital/index.php/webservice/login?login=" + login + "&senha=" + senha;
    let loading = this.loadingCtrl.create();

    loading.present();
    return new Promise(resolve => {
      this.httpClient.get(url).subscribe(
        res => {
          localStorage.setItem('loginOk', "true");
          loading.dismiss();
          resolve(res);
        },
        err => {
          loading.dismiss();
          resolve(err.error);
        }
      );
    }).catch(err => { console.log(err); loading.dismiss() });

  }

  
  salvarAluno(dados: any): Promise<any> {

    let url = "http://tl.upmidia.digital/index.php/webservice/aluno";
    //logradouro, numero, complemento, cidade, uf, cep

    url = url + "?email=" + dados.email
      + "&nome=" + dados.nome
      + "&login=" + dados.email
      + "&senha=" + dados.senha
      + "&nascimento=" + dados.nascimento
      + "&cpf=" + dados.cpf
      + "&rg=" + dados.rg
      + "&foto=" + dados.foto
      + "&enderecos[0][logradouro]=" + dados.logradouro
      + "&enderecos[0][numero]=" + dados.numero
      + "&enderecos[0][complemento]=" + 'naotem'
      + "&enderecos[0][cidade]=" + dados.cidade
      + "&enderecos[0][uf]=" + dados.uf
      + "&enderecos[0][cep]=" + dados.cep
      + "&telefones[0][ddd]=" + dados.ddd
      + "&telefones[0][numero]=" + dados.telefone
      ;
    //let body = new URLSearchParams();
    let loading = this.loadingCtrl.create({
      content: 'Validando ....'
    });

    loading.present();
    console.log(dados);

    /**
     * nome,email,nascimento,telefone,endereco,bairro,cep,cidade,
      pais,sexo,numero,cpf,rg       */


    return new Promise(resolve => {
      this.httpClient.post(url, dados.toString()).subscribe(
        res => {
          loading.dismiss();
          resolve(res);
        },
        err => {
          loading.dismiss();
          resolve(err.error);
        }
      );
    }).catch(err => { console.log(err); loading.dismiss() });

  }


}
