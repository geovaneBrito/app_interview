import { NgModule, ErrorHandler , LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule} from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import { Calendar } from '@ionic-native/calendar';

import localePt from '@angular/common/locales/pt';
registerLocaleData(localePt);


import { TabsPage } from '../pages/tabs/tabs';
import { ProdutoPage } from '../pages/produto/produto';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { ConfirmacaoPage } from '../pages/confirmacao/confirmacao';


import { NotificacaoPage } from '../pages/notificacao/notificacao';
import { CalendarPage } from '../pages/calendar/calendar';
import { InterViewServiceProvider } from '../providers/service/service';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { BrMaskerModule} from 'brmasker-ionic-3';

import { Camera } from '@ionic-native/camera';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    ProdutoPage,
    NotificacaoPage,
    CadastroPage,
    ConfirmacaoPage,
 
    CalendarPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    BrMaskerModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    CadastroPage,
    ConfirmacaoPage,
    
    ProdutoPage,
    CalendarPage,
    NotificacaoPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Calendar,
    InterViewServiceProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: LOCALE_ID, useValue: "pt-BR" },
    Camera
  ]
})
export class AppModule {}
